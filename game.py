import random
from msvcrt import getch
import os

os.system('cls')


class Games:
    def __init__(self, size):
        self.score = 0
        self.size = size
        self.board = [[0 for x in range(self.size)] for y in range(self.size)]
        self.rand()
        self.rand()
        self.print_field()

    # прорисовка поля
    def print_field(self):
        for i in range(0, self.size):
            for j in range(0, self.size):
                print(self.board[i][j], end='\t')
            print(end="\n")

    # генерация случайной 2 на поле
    def rand(self):
        count = 0
        choice_list = [0, 1, 2, 3]
        for i, row in enumerate(self.board):
            for j, item in enumerate(row):
                if item == 0:
                    count += 1
        n = random.choice(choice_list)
        m = random.choice(choice_list)
        if self.board[n][m] == 0:
            self.board[n][m] = 2
        elif count > 0:
            self.rand()

    # проверка условия выигрыша/проигрыша
    def win(self):
        count = 0
        kk = 0
        maxvalue = -999
        for i, row in enumerate(self.board):
            for j, item in enumerate(row):
                if item >= maxvalue:
                   maxvalue = item
        for i, row in enumerate(self.board):
            for j, item in enumerate(row):
                if item == 0:
                    count += 1
        for j in range(0, self.size):
            for i in range(0, self.size):
                for k in range(i + 1, self.size):
                    if self.board[i][j] != 0:
                        if self.board[i][j] == self.board[k][j]:
                            kk += 1
                    break
        for i in range(0, self.size):
            for j in range(0, self.size):
                for k in range(j + 1, self.size):
                    if self.board[i][j] != 0:
                        if self.board[i][j] == self.board[i][k]:
                            kk += 1
                    break
        if count == 0 and kk == 0:
            print("Game over")
            exit()
        if maxvalue == 2048:
            print("You win!!!")
            exit()

    # общее у функций вверх/вниз
    def up_down(self):
        for j in range(0, self.size):
            for i in range(0, self.size):
                for k in range(i + 1, self.size):
                    if self.board[k][j] != 0:
                        if self.board[i][j] == 0:
                            self.board[i][j] = self.board[k][j]
                            self.board[k][j] = 0
                        else:
                            if self.board[i][j] == self.board[k][j]:
                                self.board[i][j] += self.board[k][j]
                                self.score += self.board[k][j]*2
                                self.board[k][j] = 0
                            break

    # общее у функций влево/вправо
    def left_right(self, i):
        for j in range(0, self.size):
            for k in range(j + 1, self.size):
                if self.board[i][k] != 0:
                    if self.board[i][j] == 0:
                        self.board[i][j] = self.board[i][k]
                        self.board[i][k] = 0
                    else:
                        if self.board[i][j] == self.board[i][k]:
                            self.board[i][j] += self.board[i][k]
                            self.score += self.board[i][k]*2
                            self.board[i][k] = 0
                        break

    def move(self):
        while True:
            s = 0
            key = ord(getch())

            if key == 27:  # ESC
                break

            elif key == 75:  # стрелка влево
                os.system('cls')
                for i in range(0, self.size):
                    Games.left_right(self, i)
                self.rand()
                self.print_field()

            elif key == 77:  # стрелка вправо
                os.system('cls')
                for i in range(0, self.size):
                    self.board[i].reverse()
                    Games.left_right(self, i)
                    self.board[i].reverse()
                self.rand()
                self.print_field()

            elif key == 72:  # стрелка вверх
                os.system('cls')
                s = self.score
                self.up_down()
                self.rand()
                self.print_field()

            elif key == 80:  # стрелка вниз
                os.system('cls')
                self.board.reverse()
                self.up_down()
                self.board.reverse()
                self.rand()
                self.print_field()

            print("score ", self.score)
            self.win()

c = Games(4)
c.move()

